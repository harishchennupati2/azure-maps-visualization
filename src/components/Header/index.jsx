import React from 'react';
import { ReactComponent as User } from '../../assets/user.svg';
import './styles.scss';

const Header = () => {
  return (
    <div
      style={{
        backgroundColor: '#0082ba',
        padding: '10px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}
    >
      <div className="header-logo" />

      <div className="user-info">
        <User />
        <div className="user-details">
          <div>User 1</div>
          <div>user1@email.com</div>
        </div>
      </div>
    </div>
  );
};

export default Header;
