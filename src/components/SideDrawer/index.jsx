import React from 'react';
import './index.scss';

const SideDrawer = props => {
  return (
    <>
      <div
        style={{
          position: 'absolute',
          top: '0px',
          left: '0px',
          right: '100%',
          width: '100%',
          height: '100%',
          backgroundColor: 'black',
          zIndex: '100',
          opacity: '0.5',
          display: 'flex',
          flexDirection: 'column'
        }}
      />
      <div
        style={{
          position: 'absolute',
          top: '0px',
          right: '0px',
          height: '100vh',
          padding: '10px',
          backgroundColor: 'white',
          zIndex: '1000',
          width: '300px'
        }}
        className="animation"
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '5%'
          }}
        >
          <h3
            style={{
              margin: '0'
            }}
          >
            {props.header}
          </h3>
          <button
            style={{
              backgroundColor: '#0082ba',
              padding: '10px',
              color: 'white',
              border: 'none',
              cursor: 'pointer',
              height: '30px',
              width: '30px',
              display: 'flex',
              alignItems: 'center'
            }}
            onClick={props.onClose}
          >
            X
          </button>
        </div>
        <div
          style={{
            height: '93%'
          }}
        >
          {props.children}
        </div>
      </div>
    </>
  );
};

export default SideDrawer;
