import React, { useEffect, useState } from 'react';
import { useTable, useRowSelect, useRowState } from 'react-table';
import { ReactComponent as BackIcon } from '../../assets/arrow-left-circle.svg';
import useDeepCompareEffect from 'use-deep-compare-effect';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';

import { CustomTable as SecondaryTable } from './second';
import _ from 'lodash';
import './index.scss';

const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = React.useRef();
  const resolvedRef = ref || defaultRef;

  React.useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate;
  }, [resolvedRef, indeterminate]);

  return (
    <>
      <input type="checkbox" ref={resolvedRef} {...rest} />
    </>
  );
});

const CustomTable = ({ columns, data, showCheckbox, onChange, showAlarms, alarmCols, filters, onClick }) => {
  const [selectedRows, setSelectedRows] = useState([]);

  const [showSuburbs, setShowSuburbs] = useState(true);

  const [_rows, setRows] = useState({});

  console.log(filters);

  useEffect(() => {
    setRows([]);
    setShowSuburbs(true);
  }, []);

  let alarmsData = [];

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, selectedFlatRows, state, setRowState } = useTable(
    {
      columns,
      data
    },
    useRowSelect,
    useRowState,
    hooks => {
      showCheckbox &&
        hooks.visibleColumns.push(columns => [
          // Let's make a column for selection
          {
            id: 'selection',
            // The header can use the table's getToggleAllRowsSelectedProps method
            // to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <div>
                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              </div>
            ),
            // The cell can use the individual row's getToggleRowSelectedProps method
            // to the render a checkbox
            Cell: ({ row }) => {
              setSelectedRows(rows => (rows[row.index] = row));
              return (
                <div>
                  <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                </div>
              );
            }
          },
          ...columns
        ]);
    }
  );

  alarmsData = [];

  data.map(r => {
    if (!_.isEmpty(r) && !_.isEmpty(_rows)) {
      if (r.Suburb === _rows.original.Suburb) {
        alarmsData = r.data;
      }
    }
  });

  return (
    <>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: '15px',
          marginTop: '15px'
        }}
      >
        <h3
          style={{
            marginTop: '0px',
            marginBottom: '0px'
          }}
        >
          {showSuburbs ? (
            'Alarms By Suburb'
          ) : (
            <div className="alarms-back">
              Alarms
              <span
                onClick={() => {
                  setShowSuburbs(!showSuburbs);
                  setRows([]);
                }}
              >
                <BackIcon /> Go Back
              </span>
            </div>
          )}
        </h3>
        <button
          style={{
            backgroundColor: '#0082ba',
            padding: '10px',
            color: 'white',
            border: 'none',
            cursor: 'pointer',
            fontWeight: 'bold'
          }}
          onClick={() => {
            onClick();
          }}
        >
          Apply Filters
        </button>
      </div>
      {showSuburbs && (
        <table {...getTableProps()} className="responsiveTable">
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    console.log(cell);
                    if (cell.column.Header === 'Suburb') {
                      return (
                        <td
                          {...cell.getCellProps()}
                          style={{
                            cursor: 'pointer'
                          }}
                          onClick={event => {
                            setRows(cell.row);
                            setShowSuburbs(!showSuburbs);
                          }}
                        >
                          {cell.render('Cell')}
                        </td>
                      );
                    }
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      )}

      {showAlarms && !showSuburbs && rows.length > 0 && (
        <div className="alarms">
          <SecondaryTable columns={alarmCols} data={alarmsData} />
        </div>
      )}
    </>
  );
};

export default React.memo(CustomTable);
