import React, { useEffect, useState, useRef } from 'react';
import moment from 'moment';
import * as atlas from 'azure-maps-control';
import * as atlasd from 'azure-maps-drawing-tools';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import 'azure-maps-control/dist/atlas.min.css';
import 'azure-maps-drawing-tools/dist/atlas-drawing.min.css';
import useForceUpdate from 'use-force-update';

import _ from 'lodash';

import { intersect } from '@turf/turf';

import CustomTable from '../../components/Table';
import SideDrawer from '../../components/SideDrawer';
import './index.scss';
import ResizePanel from 'react-resize-panel';
import { withResizeDetector } from 'react-resize-detector';
import { matchPath } from 'react-router-dom';
let data = require('../../mock-data/alarms.json');

let map;

let popup;

const resizeComp = ({ width, height }) => {
  // eslint-disable-next-line
  useEffect(() => {
    if (map) map.resize();
  }, [width]);

  return (
    <ResizePanel
      direction="e"
      style={{
        height: '100vh',
        flex: '2'
      }}
    >
      <div
        id="myMap"
        style={{
          width: '100%'
        }}
        className="map-resizable"
      ></div>
    </ResizePanel>
  );
};

const AdaptiveWithDetector = withResizeDetector(resizeComp);

function Map(props) {
  const [showFilters, setShowFilters] = useState(false);
  const [isDrawn, setIsDrawn] = useState(false);

  const [filters, setFilters] = useState({
    alarmStatus: '',
    ackStatus: '',
    fromDate: null,
    toDate: null
  });

  const forceUpdate = useForceUpdate();

  const [mockData, setMockData] = useState(data);

  const [showClear, setShowClear] = useState(false);

  const [alarmsCount, setAlarmsCount] = useState(0);

  const [applyFilters, setApplyFilters] = useState(false);

  let features = useRef([]);

  let clusterBubbleLayer, iconLayer;
  let drawingManager = useRef({});

  let dataSource = useRef({});

  let updatedFilterData = useRef([]);

  const loadData = mockData => {
    features.current = [];
    mockData.map(point => {
      features.current.push(
        new atlas.data.Feature(new atlas.data.Point([point['geolong'], point['geolat']]), {
          suburb: point['suburb'],
          Alarm_Event_Status: point['Alarm_Event_Status'],
          Alarm_Event_Acknowledgment_Status: point['Alarm_Event_Acknowledgment_Status'],
          description: point['Alarm_Event_Description'],
          days: moment(point['Alarm_Event_Date']).diff(moment(), 'days') * -1,
          priority: point['Alarm_Event_Priority'],
          address: point['devadd'],
          device_id: point['device_id'],
          serno: point['serno'],
          color:
            point['Alarm_Event_Status'].toLowerCase() === 'active' ||
            point['Alarm_Event_Acknowledgment_Status'].toLowerCase() === 'acknowledged'
              ? 'pin-round-blue'
              : 'pin-red'
        })
      );
    });

    dataSource.current && dataSource.current.setShapes(features.current);
  };

  const updateListItems = () => {
    const appliedFilters = JSON.parse(localStorage.getItem('filters'));
    const camera = map.getCamera();

    const shapes = map.layers.getRenderedShapes(map.getCamera().bounds, [iconLayer]);

    const shapeProperties = [];

    let canUpdate = false;

    shapes.forEach(shape => {
      if (shape instanceof atlas.Shape) {
        canUpdate = true;
        shapeProperties.push(shape.getProperties());
      }
    });

    if (canUpdate) {
      setMockData(shapeProperties);
    } else {
      if (areFiltersApplied(appliedFilters)) {
        setMockData(updatedFilterData.current);
      } else {
        setMockData(data);
      }
    }
  };
  const showPopUp = shape => {
    const properties = shape.getProperties();

    const html = `
              <div class='popup-header'>
                <h4>${properties['suburb']}</h4>
                <div class='popup-status'>
                    <div><span class='status'>Status - </span> ${properties['Alarm_Event_Status']}</div>
                    <div><span class='status'>Ack Status - </span> ${properties['Alarm_Event_Acknowledgment_Status']}</div>
                </div>
              </div>
              <div class='popup-body'>
                <div class="d-flex">
                    <p class='description'>${properties['description']}</p>
                    <p class='priority'>
                      <span>Priority: </span>
                      ${properties['priority']}
                     </p>
                </div>
                <div class='popup-date'>
                  <p>${properties['days']} days ago</p>
                </div>
                <p class='address'>${properties['address']}</p>
              </div>
    `;

    popup.setOptions({
      content: html,
      position: shape.getCoordinates()
    });

    popup.open(map);
  };

  function drawingModeChanged(mode) {
    //Clear the drawing canvas when the user enters into a drawing mode and reset the style of all pins.
    if (mode.startsWith('draw')) {
      drawingManager.current.getSource().clear();

      //Reset the color property of each point.
      for (var i = 0; i < features.current.length; i++) {
        features.current[i].properties.color =
          features.current[i].properties['Alarm_Event_Status'].toLowerCase() === 'active' ||
          features.current[i].properties['Alarm_Event_Acknowledgment_Status'].toLowerCase() === 'acknowledged'
            ? 'pin-round-blue'
            : 'pin-red';
      }

      setIsDrawn(false);

      setShowClear(false);

      // dataSource.current.clear();

      //Update the data on the map.
      dataSource.current.setShapes(features.current);
    }
  }

  function searchPolygon(searchArea) {
    //Exit drawing mode.
    drawingManager.current.setOptions({ mode: 'idle' });

    var selectedPoints = [];
    var poly = searchArea.toJson();

    //If the search area is a circle, create a polygon from its circle coordinates.
    if (searchArea.isCircle()) {
      poly = new atlas.data.Polygon([searchArea.getCircleCoordinates()]);
    }

    let count;

    //Search for all points that intersect the polygon.
    for (var i = 0; i < features.current.length; i++) {
      var intersection = intersect(poly, features.current[i]);

      if (intersection) {
        selectedPoints.push(features.current[i]);

        //Change the color of the point.
        features.current[i].properties.color =
          features.current[i].properties['Alarm_Event_Status'].toLowerCase() === 'active' ||
          features.current[i].properties['Alarm_Event_Acknowledgment_Status'].toLowerCase() === 'acknowledged'
            ? 'pin-round-darkblue'
            : 'pin-darkblue';
      }
    }
    count = new Set(selectedPoints.map(point => point.geometry.coordinates[0] + point.geometry.coordinates[1])).size;
    setIsDrawn(true);
    setShowClear(true);
    setAlarmsCount(count);
  }

  const areFiltersApplied = filters => {
    if (_.isEmpty(filters)) {
      return false;
    }
    if (!_.isEmpty(filters.alarmStatus) || !_.isEmpty(filters.ackStatus) || !_.isNull(filters.fromDate) || !_.isNull(filters.toDate)) {
      return true;
    }
    return false;
  };

  useEffect(() => {
    map = new atlas.Map('myMap', {
      center: [145.1357, -38.1466],
      zoom: 13,
      language: 'en-US',
      authOptions: {
        authType: 'subscriptionKey',
        subscriptionKey: 'IeEIOEd-QnykYWSsT27WprySyt47rCkfrtB6dmGmwRQ'
      },
      autoResize: false
    });

    //Create a popup but leave it closed so we can update it and display it later.
    popup = new atlas.Popup();

    map.events.add('ready', function () {
      //Create a drawing toolbar.
      let drawingToolbar = new atlasd.control.DrawingToolbar({
        buttons: ['draw-rectangle', 'draw-circle'],
        position: 'top-left',
        style: 'light'
      });

      //Create an instance of the drawing manager and display the drawing toolbar.
      drawingManager.current = new atlasd.drawing.DrawingManager(map, {
        toolbar: drawingToolbar
      });
      // Add Zoom control to the map

      map.controls.add(new atlas.control.ZoomControl(), {
        position: 'top-right'
      });

      //Create a data source and add it to the map and enable clustering.
      dataSource.current = new atlas.source.DataSource(null, {
        cluster: true
        // clusterMaxZoom: 15
      });

      map.sources.add(dataSource.current);

      loadData(mockData);

      clusterBubbleLayer = new atlas.layer.BubbleLayer(dataSource.current, null, {
        radius: 18,
        color: '#007faa',
        strokeColor: 'white',
        strokeWidth: 2,
        filter: ['has', 'point_count'] //Only render data points which have a point_count property, which clusters do.
      });

      var clusterLabelLayer = new atlas.layer.SymbolLayer(dataSource.current, null, {
        iconOptions: {
          image: 'none' //Hide the icon image.
        },
        textOptions: {
          textField: ['get', 'point_count_abbreviated'],
          size: 12,
          font: ['StandardFont-Bold'],
          offset: [0, 0.4],
          color: 'white'
        }
      });

      iconLayer = new atlas.layer.SymbolLayer(dataSource.current, null, {
        iconOptions: {
          image: ['get', 'color']
        },
        filter: ['!', ['has', 'point_count']] //Filter out clustered points from this layer.
      });
      map.layers.add([clusterBubbleLayer, clusterLabelLayer, iconLayer]);

      //Create an instance of the drawing manager and display the drawing toolbar.
      // drawingManager = new atlasd.drawing.DrawingManager(map, {
      //   toolbar:
      // });
      //When the mouse is over the cluster and icon layers, change the cursor to be a pointer.
      map.events.add('mouseover', [clusterBubbleLayer, iconLayer], function () {
        map.getCanvasContainer().style.cursor = 'pointer';
      });

      //When the mouse leaves the item on the cluster and icon layers, change the cursor back to the default which is grab.
      map.events.add('mouseout', [clusterBubbleLayer, iconLayer], function () {
        map.getCanvasContainer().style.cursor = 'grab';
      });

      //Add a click event to the cluster layer. When someone clicks on a cluster, zoom into it by 2 levels.
      map.events.add('click', clusterBubbleLayer, function (e) {
        map.setCamera({
          center: e.position,
          zoom: map.getCamera().zoom + 2
        });
      });

      //Add a click event to the icon layer and show the shape that was clicked.
      map.events.add('click', iconLayer, function (e) {
        showPopUp(e.shapes[0]);
      });
    });

    //Add an event to monitor when the map has finished moving.
    map.events.add('render', event => {
      //Give the map a chance to move and render data before updating the list.
      updateListItems();

      //Hide the polygon fill area as only want to show outline of search area.
      drawingManager.current && drawingManager.current.getLayers().polygonLayer.setOptions({ visible: false });

      // //Clear the map and drawing canvas when the user enters into a drawing mode.
      drawingManager.current && map.events.add('drawingmodechanged', drawingManager.current, drawingModeChanged);

      // //Monitor for when a polygon drawing has been completed.
      drawingManager.current && map.events.add('drawingcomplete', drawingManager.current, searchPolygon);
    });

    // /*Ensure that the map is fully loaded*/
    // map.events.add('load', function () {
    //   /*Add point locations*/
    //   // var points = [
    //   //   new atlas.data.Point([144.946457, -37.840935]),
    //   //   new atlas.data.Point([138.599503, -34.92123]),
    //   //   new atlas.data.Point([147.157135, -41.429825]),
    //   //   new atlas.data.Point([115.857048, -31.953512])
    //   // ];

    //   /*Create a data source and add it to the map*/
    //   var dataSource = new atlas.source.DataSource();
    //   map.sources.add(dataSource);
    //   /*Add multiple points to the data source*/
    //   // dataSource.add(points);
    //   dataSource.importDataFromUrl('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson');

    //   //Create a bubble layer to render the filled in area of the circle, and add it to the map.*/
    //   map.layers.add(
    //     new atlas.layer.BubbleLayer(dataSource, null, {
    //       radius: 5,
    //       strokeColor: '#4288f7',
    //       strokeWidth: 6,
    //       color: 'white'
    //     })
    //   );

    //   map.layers.add(
    //     new atlas.layer.HeatMapLayer(dataSource, null, {
    //       radius: 5,
    //       opacity: 0.8
    //     })
    //   );
    // });
  }, []);

  useEffect(() => {
    localStorage.clear();
  }, []);

  useEffect(() => {
    !_.isEmpty(dataSource.current) && dataSource.current.setShapes(features.current);
  }, [isDrawn]);

  // const columns = [
  //   {
  //     Header: 'Suburb',
  //     accessor: 'suburb'
  //   },
  //   {
  //     Header: 'Length',
  //     accessor: 'length'
  //   }
  // ];

  useEffect(() => {
    let filteredData = data;

    if (!_.isEmpty(filters.alarmStatus)) {
      filteredData = filteredData.filter(row => {
        if (row['Alarm_Event_Status'].toLowerCase() === filters.alarmStatus.toLowerCase()) {
          return true;
        }
      });
    }

    if (!_.isEmpty(filters.ackStatus)) {
      filteredData = filteredData.filter(row => {
        if (row['Alarm_Event_Acknowledgment_Status'].toLowerCase() === filters.ackStatus.toLowerCase()) {
          return true;
        }
      });
    }

    if (!_.isNull(filters.fromDate) && !_.isNull(filters.toDate)) {
      const fromDate = moment(filters.fromDate);
      const toDate = moment(filters.toDate);

      filteredData = filteredData.filter(row => {
        const currentDate = moment(row['Alarm_Event_Date']);
        return (
          (currentDate.isBefore(toDate) && currentDate.isAfter(fromDate)) || currentDate.isSame(fromDate) || currentDate.isSame(toDate)
        );
      });
    }

    if (applyFilters && areFiltersApplied(filters)) {
      setMockData(filteredData);
      localStorage.setItem('filters', JSON.stringify(filters));
      loadData(filteredData);
      updatedFilterData.current = filteredData;
    }
  }, [filters, applyFilters]);

  const grouped = _.groupBy(mockData, 'suburb');

  const tableGrouped = React.useMemo(
    () =>
      Object.keys(grouped).map(key => {
        return {
          Suburb: key,
          number: grouped[key].length,
          data: grouped[key]
        };
      }),
    [grouped]
  );

  const columns = React.useMemo(
    () => [
      {
        Header: 'Suburb',
        accessor: 'Suburb'
      },
      {
        Header: 'No of Alarms',
        accessor: 'number'
      }
    ],
    []
  );

  const alarmCols = [
    {
      Header: 'Device Id',
      accessor: 'device_id'
    },
    {
      Header: 'Serial No',
      accessor: 'serno'
    },
    {
      Header: 'Suburb',
      accessor: 'suburb'
    },
    {
      Header: 'Status',
      accessor: 'Alarm_Event_Status'
    },
    {
      Header: 'Ack Status',
      accessor: 'Alarm_Event_Acknowledgment_Status'
    }
  ];

  return (
    <div
      style={{
        display: 'flex'
      }}
    >
      <AdaptiveWithDetector />
      <div
        style={{
          flex: '1',
          paddingLeft: '10px',
          paddingRight: '10px'
        }}
      >
        <CustomTable
          columns={columns}
          data={tableGrouped}
          showAlarms={true}
          alarmCols={alarmCols}
          filters={areFiltersApplied(JSON.parse(localStorage.getItem('filters')))}
          onClick={() => {
            setShowFilters(true);
            setApplyFilters(false);
          }}
        />

        {showClear && (
          <button
            className="clear-btn"
            onClick={() => {
              drawingManager.current.getSource().clear();
              drawingManager.current.setOptions({ mode: 'idle' });

              for (var i = 0; i < features.current.length; i++) {
                features.current[i].properties.color =
                  features.current[i].properties['Alarm_Event_Status'].toLowerCase() === 'active' ||
                  features.current[i].properties['Alarm_Event_Acknowledgment_Status'].toLowerCase() === 'acknowledged'
                    ? 'pin-round-blue'
                    : 'pin-red';
              }

              setIsDrawn(false);

              // dataSource.current.clear();

              //Update the data on the map.
              dataSource.current.setShapes(features.current);
              setShowClear(false);
            }}
          >
            <span>&#10005;</span>Clear Selection
          </button>
        )}
        {showClear && (
          <button className="alarm-cnt-btn">
            <span>
              {alarmsCount}
              {'  '}
              {alarmsCount > 1 ? 'Alarms Selected' : 'Alarm Selected'}
            </span>
          </button>
        )}
        {showFilters && (
          <SideDrawer
            header={'Apply Filters'}
            onClose={() => {
              setShowFilters(!showFilters);
            }}
          >
            <div className="filters">
              <div className="status">
                <div
                  className="status-label"
                  onChange={event => {
                    setFilters(prev => ({
                      ...prev,
                      [event.target.name]: event.target.value
                    }));
                  }}
                >
                  <p>Alarm Status</p>
                  <div>
                    <input
                      type="radio"
                      name="alarmStatus"
                      id="alarmStatus"
                      value="ACTIVE"
                      checked={filters.alarmStatus.toLowerCase() === 'active'}
                    />
                    <label for="alarmStatus">Active</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="alarmStatus"
                      id="alarmStatusCleared"
                      value="CLEARED"
                      checked={filters.alarmStatus.toLowerCase() === 'cleared'}
                    />
                    <label for="alarmStatusCleared">Cleared</label>
                  </div>
                </div>
                <div
                  className="status-label"
                  onChange={event => {
                    setFilters(prev => ({
                      ...prev,
                      [event.target.name]: event.target.value
                    }));
                  }}
                >
                  <p>Alarm Ack Status</p>
                  <div>
                    <input
                      type="radio"
                      name="ackStatus"
                      id="ackStatus"
                      value="ACKNOWLEDGED"
                      checked={filters.ackStatus.toLowerCase() === 'acknowledged'}
                    />
                    <label for="ackStatus">Acknowledged</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="ackStatus"
                      id="ackStatusUn"
                      value="UNACKNOWLEDGED"
                      checked={filters.ackStatus.toLowerCase() === 'unacknowledged'}
                    />
                    <label for="ackStatusUn">UnAcknowledged</label>
                  </div>
                </div>
              </div>
              <div className="date-filters">
                <div>
                  <p>From Date</p>
                  <DatePicker
                    selected={!_.isNull(filters.fromDate) ? new Date(filters.fromDate) : null}
                    onChange={date => {
                      setFilters(prev => ({
                        ...prev,
                        fromDate: !_.isNull(date) ? moment(date).format('YYYY-MM-DD') : null
                      }));
                    }}
                    placeholderText="MM/DD/YYYY"
                    isClearable
                  />
                </div>
                <div>
                  <p> To Date</p>
                  <DatePicker
                    selected={!_.isNull(filters.toDate) ? new Date(filters.toDate) : null}
                    onChange={date =>
                      setFilters(prev => ({
                        ...prev,
                        toDate: !_.isNull(date) ? moment(date).format('YYYY-MM-DD') : null
                      }))
                    }
                    placeholderText="MM/DD/YYYY"
                    isClearable
                  />
                </div>
              </div>

              <div className="bottom">
                <button
                  style={{
                    backgroundColor: '#0082ba',
                    padding: '10px',
                    color: 'white',
                    border: 'none',
                    cursor: 'pointer',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                  onClick={() => {
                    setShowFilters(!showFilters);
                    setApplyFilters(true);
                  }}
                >
                  Apply Filters
                </button>
                <button
                  style={{
                    backgroundColor: 'white',
                    color: '#0082ba',
                    fontWeight: 'bold',
                    padding: '10px',
                    cursor: 'pointer',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                  className="clear"
                  onClick={() => {
                    setFilters({
                      alarmStatus: '',
                      ackStatus: '',
                      fromDate: null,
                      toDate: null
                    });
                    setMockData(data);
                    loadData(data);
                    localStorage.clear();

                    // setShowFilters(!showFilters);
                  }}
                >
                  Clear Filters
                </button>
              </div>
            </div>
          </SideDrawer>
        )}
      </div>
    </div>
  );
}

Map.propTypes = {};

export default Map;
